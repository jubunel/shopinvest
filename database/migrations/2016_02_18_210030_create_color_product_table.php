<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_product', function(Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('color_id')->references('id')->on('colors')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_color', function(Blueprint $table) {
            $table->dropForeign('product_color_product_id_foreign');
            $table->dropForeign('product_color_color_id_foreign');
        });

        Schema::drop('product_color');
    }
}
