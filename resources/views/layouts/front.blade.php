<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Julien Bunel">

    <title>@yield('title')</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/bootstrap-slider.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="header">
            <a class="navbar-brand" href="/">LOGO</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/cart">Panier <br />@yield('cart')</a></li>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
    <div class="container">
        @yield('breadcrumb')
    </div>


    <div class="container">
        @yield('content')
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="/js/bootstrap-slider.min.js"></script>

    <script>
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('img.miniature').click(function (e) {
            var miniSrc = $(this).attr('src');
            var mainSrc = $("#main").attr('src');

            $("#main").attr('src', miniSrc);
            $(this).attr('src', mainSrc);
        });

        function swap(image) {
            document.getElementById("main").src = image.href;
        }

        $( document ).ready(function() {
            var qte;

            $('a#plus').click(function() {
                qte = parseInt($('a#qte').text());
                qte += 1;
                $('a#qte').text(qte)
            });

            $('a#minus').click(function() {
                qte = parseInt($('a#qte').text());
                if(qte > 1) qte -= 1;
                $('a#qte').text(qte)
            });

            $('#addToCart').click(function(e){
                e.preventDefault();

                var href = $(this).attr('href');
                var url = href + qte;
                window.location.href = url;
            });

            $("#price").slider({

            });

            $("#price").on("slide", function(slideEvt) {
                var min = slideEvt.value[0];
                var max = slideEvt.value[1];

                $("b.min-price").text(min + "€");
                $("b.max-price").text(max + "€");
            });

            var priceValues = JSON.parse($("#price").attr("data-slider-value"));
            $("b.min-price").text(priceValues[0] + "€");
            $("b.max-price").text(priceValues[1] + "€");
        });
    </script>
</body>
</html>