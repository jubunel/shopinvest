<?php

namespace App\Http\Controllers;

use App\Picture;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Flysystem\Exception;

class PicturesController extends Controller
{
    //
    public function destroy($id) {
        $picture = Picture::find($id);

        Picture::destroy($id);

        try {
            unlink(public_path() . '/uploads/' . $picture->filename);
        } catch(Exception $e) {

        }

        return redirect('/admin/products/' . $picture->product_id. '/edit');
    }
}
