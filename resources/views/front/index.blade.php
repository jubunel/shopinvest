@extends('layouts.front')

@section('title', 'Catalogue')

@section('cart')
    @if($total == 0)
        Aucun article
    @else
        {{ $total }} article(s)
    @endif
@endsection

@section('breadcrumb')
<ol class="breadcrumb">
  <li class="active">Accueil</li>
</ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-md-3">
                <div class="facettes">
                    {!! Form::open(array('action' => 'MainController@search', 'method' => 'post')) !!}
                    <div class="form-group">
                        {!! Form::label('brand', 'Marque') !!}
                        {!! Form::select('brand', $brandsSelect, null, array('class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Prix') !!} : <b class="min-price">{{ $minPrice }}€</b> - <b class="max-price">{{ $maxPrice }}€</b>
                        <div>
                            {!! Form::text('price', null, array('id' => 'price', 'class' => 'form-control', 'data-slider-min' => $minPrice, 'data-slider-max' => $maxPrice, 'data-slider-step' => 5, 'data-slider-value' => '['.$minPrice.','.$maxPrice.']', 'data-slider-tooltip-split' => 'true', 'data-slider-tooltip' => 'hide')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('color', 'Couleur') !!}
                        {!! Form::select('colors', $colorsSelect, null, array('class' => 'form-control')) !!}
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('stock', true, false) !!}
                            En stock
                        </label>
                    </div>
                    {!! Form::submit('Rechercher', array('class' => 'btn btn-primary btn-lg btn-block')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-9">
                @foreach ($products as $product)
                    <div class="col-sm-6 col-md-4 product">
                        <div class="thumbnail" >
                            @if(sizeof($product->pictures) > 0)
                                <img src="uploads/{{$product->pictures{0}->filename}}" class="img-responsive">
                            @endif
                            <div class="caption">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>{{$product->name}}</h3>
                                    </div>
                                </div>
                                <p class="description">{{$product->description}}</p>

                                <div class="row">
                                    <div class="col-md-8 stock">
                                        <div class="colors">
                                            Coloris :
                                            @for ($i = 0; $i < count($product->colors); $i++)
                                                {{$product->colors{$i}->name}}@if($i+1 != count($product->colors)),@endif
                                            @endfor
                                        </div>
                                        En stock : {{ !empty((bool)$product->stock) ? "Oui" : "Non"  }}
                                    </div>
                                    <div class="col-md-4 price">
                                        <h3><label>{{$product->price}}€</label></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="/product/detail/{{$product->id}}" class="btn btn-success btn-product btn-lg btn-block"><span class="fa fa-shopping-cart"></span> Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
@endsection