@extends('layouts.back')

@section('title', 'Editer un produit')

@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="/admin/products">Produits <span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Edition du produit : {{ $product->name  }}</h1>
            {!! Form::model($product, array('route' => array('admin.products.update', $product->id),'method'=>'PATCH', 'files'=>true, 'class'=>'form-horizontal', 'role'=>'form')) !!}
            {!! csrf_field() !!}
            <fieldset>
                <!-- Text input-->
                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                    {!! Form::label('name', 'Nom du produit', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Nom du produit']) !!}
                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                    {!! Form::label('price', 'Prix', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('price', null, ['class' => 'form-control input-md', 'placeholder' => 'Prix']) !!}
                        {!! $errors->first('price', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group {!! $errors->has('brand_name') ? 'has-error' : '' !!}">
                    {!! Form::label('brand_name', 'Marque', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('brand_name', $product->brand->name, ['class' => 'form-control input-md', 'placeholder' => 'Marque']) !!}
                        {!! $errors->first('brand_name', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                    {!! Form::label('textarea', 'Description', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group {!! $errors->has('images') ? 'has-error' : '' !!}">
                    {!! Form::label('images', 'Images', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::file('images[]', array('multiple'=>true)) !!}
                        {!! $errors->first('images', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group {!! $errors->has('colors') ? 'has-error' : '' !!}">
                    {!! Form::label('colors', 'Coloris', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">

                        {!! Form::text('colors',$colorValue , ['class' => 'form-control input-md', 'placeholder' => 'Coloris']) !!}
                        {!! $errors->first('colors', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label class="col-md-3 control-label" for="stock"></label>
                        <div class="col-md-9">
                            <label>
                                {!! Form::checkbox('stock', true, $product->stock) !!}
                                En stock
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                        @if($product->pictures)
                            @foreach($product->pictures as $picture)
                                <a href="/admin/pictures/{{$picture->id}}/destroy" class="destroy"><img src="/uploads/{{ $picture->filename }}" style="max-width:100px;"/><span style="position:absolute">Delete this file</span></a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="submit"></label>
                    <div class="col-md-9">
                        {!! Form::submit('Mettre à jour le produit', ['class' => 'btn btn-primary']) !!}
                        {!! Form::hidden('brand_id', null) !!}

                    </div>
                </div>
                {!! Form::close() !!}
            </fieldset>

            </form>
        </div>
    </div>
@endsection