@extends('layouts.back')

@section('title', 'Backoffice')

@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="/admin/products">Produits <span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Produits</h1>
            @if(Session::has('flash_message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">{{Session::get('flash_message')}}</div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <a href="/admin/products/create"><button class="btn btn-primary">Ajouter un produit</button></a>
                </div>
                <div class="col-md-6">
                    <a href="/admin/products/index"><button class="btn btn-primary" style="float: right;">Indexer le(s) produit(s)</button></a>
                </div>
            </div>


            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <td>Nom</td>
                    <td>Prix</td>
                    <td>Marque</td>
                    <td>Description</td>
                    <td>Image(s)</td>
                    <td>Coloris</td>
                    <td>En stock</td>
                    <td>&nbsp;</td>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{$product->name}}</td>
                            <td>{{$product->price}}€</td>
                            <td>{{$product->brand->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>
                                @foreach ($product->pictures as $picture)
                                    <img src="/uploads/{{$picture->filename}}" width="30px" ;/>
                                @endforeach
                            </td>
                            <td>
                                @for ($i = 0; $i < count($product->colors); $i++)
                                    {{$product->colors{$i}->name}}@if($i+1 != count($product->colors)),@endif

                                @endfor
                            </td>
                            <td>{{ !empty((bool)$product->stock) ? "Oui" : "Non"  }}</td>
                            <td><a href="/admin/products/{{$product->id}}/edit"><button class="btn btn-danger">Modifier</button></a> </td>
                            <td><a href="/admin/products/{{$product->id}}/destroy"><button class="btn btn-danger">Supprimer</button></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! $links !!}
                </div>
            </div>
        </div>
    </div>
@endsection