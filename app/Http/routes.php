<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    /* Front */
    Route::get('/', 'MainController@index');
    Route::get('/product/detail/{id}', 'MainController@show');
    Route::get('/cart/add/{id}/{qty}', 'MainController@addItem');
    Route::post('/search', 'MainController@search');

    /* AUTH */
    // Authentication routes...
    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);

    Route::get('/auth/logout', 'AuthController@getLogout');
});


Route::group(['middleware' => ['web', 'auth']], function () {
    /* Back */
    Route::get('/admin/products', 'ProductController@index');
    Route::get('/admin/products/create', 'ProductController@create');

    Route::post('/admin/products/store', [
        'as' => 'admin.products.store',
        'uses' => 'ProductController@store'
    ]);

    Route::get('/admin/products/{id}/destroy', 'ProductController@destroy');

    Route::get('/admin/products/{id}/edit', 'ProductController@edit');
    Route::patch('/admin/products/{id}', [
        'as' => 'admin.products.update',
        'uses' => 'ProductController@update'
    ]);
    Route::get('/admin/products/index', 'ProductController@createProductsIndex');




    Route::get('/admin/pictures/{id}/destroy', 'PicturesController@destroy');

    Route::get('/admin/colors', [
        'as' => 'color.index',
        'uses' =>'ColorController@index']
    );

    Route::get('/admin/colors/create', 'ColorController@create');
    Route::post('/admin/colors/store', [
        'as' => 'color.store',
        'uses' =>'ColorController@store']
    );

    Route::get('/admin/brands', [
            'as' => 'brand.index',
            'uses' =>'BrandController@index']
    );

    Route::get('/admin/brands/create', 'BrandController@create');
    Route::post('/admin/brands/store', [
            'as' => 'brand.store',
            'uses' =>'BrandController@store']
    );
});
