@extends('layouts.front')

@section('title', 'Catalogue')

@section('cart')
    @if($total == 0)
        Aucun article
    @else
        {{ $total }} article(s)
    @endif
@endsection

@section('breadcrumb')
<ol class="breadcrumb">
  <li class="active">Accueil</li>
</ol>
@endsection

@section('content')
        <div class="row">
            <div class="col-md-3">
                <div class="facettes">
                    {!! Form::open(array('action' => 'MainController@search', 'method' => 'post')) !!}
                    <div class="form-group">
                        {!! Form::label('brand', 'Marque') !!}
                        {!! Form::select('brand', $brandsSelect, $inputs["brand"], array('class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Prix') !!} : <b class="min-price">{{ $minPrice }}€</b> - <b class="max-price">{{ $maxPrice }}€</b>
                        <div>
                            {!! Form::text('price', '[' . $inputs['price'] . ']', array('id' => 'price', 'class' => 'form-control', 'data-slider-min' => $minPrice, 'data-slider-max' => $maxPrice, 'data-slider-step' => 5, 'data-slider-value' => '[' . $inputs['price'] . ']', 'data-slider-tooltip-split' => 'true', 'data-slider-tooltip' => 'hide')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('color', 'Couleur') !!}
                        {!! Form::select('colors', $colorsSelect, $inputs['colors'], array('class' => 'form-control')) !!}
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('stock', true, isset($inputs['stock'])) !!}
                            En stock
                        </label>
                    </div>
                    {!! Form::submit('Rechercher', array('class' => 'btn btn-primary btn-lg btn-block')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-9">
                @if(sizeof($products > 0))
                    @foreach ($products as $product)
                        <div class="col-sm-6 col-md-4 product">
                            <div class="thumbnail" >
                                @if(sizeof($product["_source"]["pictures"]) > 0)
                                    <img src="uploads/{{$product["_source"]["pictures"][0]["filename"]}}" class="img-responsive">
                                @endif
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>{{$product["_source"]["name"]}}</h3>
                                        </div>
                                    </div>
                                    <p class="description">{{$product["_source"]["description"]}}</p>
                                    <div class="row">
                                        <div class="col-md-8 stock">
                                            <div class="colors">
                                                Coloris :
                                                @for ($i = 0; $i < count($product["_source"]["colors"]); $i++)
                                                    {{ $product["_source"]["colors"][$i] }}@if($i+1 != count($product["_source"]["colors"])),@endif
                                                @endfor
                                            </div>
                                            En stock : {{ !empty((bool)$product["_source"]["stock"]) ? "Oui" : "Non"  }}
                                        </div>
                                        <div class="col-md-4 price">
                                            <h3><label>{{$product["_source"]["price"]}}€</label></h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="/product/detail/{{$product["_source"]["id"]}}" class="btn btn-success btn-product btn-lg btn-block"><span class="fa fa-shopping-cart"></span> Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
@endsection