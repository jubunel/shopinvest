<?php
namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\Picture;
use App\Color;

use App\Lib\Repositories\ProductRepository;
use App\Lib\Repositories\ColorRepository;

use App\Http\Requests\ProductRequest;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Elasticsearch;

class ProductController extends Controller {

    protected $productRepository;
    protected $nbrPerPage = 10;

    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    public function index() {
        //$products = Product::all();
        $products = $this->productRepository->getWithBrandAndColorsAndPicturesPaginate($this->nbrPerPage);
        $links = $products->setPath('')->render();
        return view('products.index',compact('products', 'links'));
    }

    public function create() {
        return view('products.create');
    }

    public function store(ProductRequest $request, ColorRepository $colorRepository) {
        $inputs = $request->all();

        if(!isset($inputs['stock'])) {
            $inputs = array_merge($inputs, ['stock' => false]);
        } else {
            $inputs = array_merge($inputs, ['stock' => true]);
        }

        /* Brand */
        if(isset($inputs['brand_name'])) {
            $brand = Brand::firstOrCreate(array('name' => $inputs['brand_name']));
            $brand->save();
        }
        $inputs = array_merge($inputs, ['brand_id' => $brand->id]);

        /* Product */
        $product = $this->productRepository->store($inputs);

        /* Colors */
        if(isset($inputs['colors'])) {
            $colorRepository->store($product, $inputs['colors']);
        }

        /* Pictures */
        if(!is_null($inputs['images'][0])) {
            $files = Request::file('images');
            $file_count = count($files);

            if ($file_count > 0) {
                foreach ($files as $file) {
                    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                    $validator = Validator::make(array('file' => $file), $rules);
                    if ($validator->passes()) {
                        $destinationPath = 'uploads';
                        $extension = $file->getClientOriginalExtension();
                        $filename = $file->getFilename();
                        $upload_success = $file->move($destinationPath, $filename . '.' . $extension);

                        if ($upload_success) {
                            $picture = new Picture();
                            $picture->filename = $filename . '.' . $extension;
                            $picture->product_id = $product->id;
                            $picture->save();
                        }
                    }
                }
            }
        }

        return redirect('/admin/products');
    }

    public function edit($id) {
        $product = Product::findOrFail($id);
        $colorValue = "";
        $colors = $product->colors;

        foreach($product->colors as $color) {
                $colorValue .= $color->name . ",";
            }

        $colorValue = substr($colorValue, 0, -1);

        return view('products.edit', compact('product', 'colorValue'));
    }

    public function update($id, ProductRequest $request, ColorRepository $colorRepository) {
        $inputs = $request->all();

        if(!isset($inputs['stock'])) {
            $inputs = array_merge($inputs, ['stock' => false]);
        } else {
            $inputs = array_merge($inputs, ['stock' => true]);
        }

        /* Brand */
        if(isset($inputs['brand_name'])) {
            $brand = Brand::firstOrCreate(array('name' => $inputs['brand_name']));
            $brand->save();
        }

        $inputs = array_merge($inputs, ['brand_id' => $brand->id]);

        /* Product */
        $this->productRepository->update($id, $inputs);

        $product = $this->productRepository->getById($id);

        /* Colors */
        if(isset($inputs['colors'])) {
            $product->colors()->detach();
            $colorRepository->store($product, $inputs['colors']);
        }

        /* Pictures */
        if(!is_null($inputs['images'][0])) {
            $files = Request::file('images');
            $file_count = count($files);

            if ($file_count > 0) {
                foreach ($files as $file) {
                    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                    $validator = Validator::make(array('file' => $file), $rules);
                    if ($validator->passes()) {
                        $destinationPath = 'uploads';
                        $extension = $file->getClientOriginalExtension();
                        $filename = $file->getFilename();
                        $upload_success = $file->move($destinationPath, $filename . '.' . $extension);

                        if ($upload_success) {
                            $picture = new Picture();
                            $picture->filename = $filename . '.' . $extension;
                            $picture->product_id = $product->id;
                            $picture->save();
                        }
                    }
                }
            }
        }

        \Session::flash('flash_message', 'Le produit '.$product->name.' vient d\'être mis à jour');
        return redirect('/admin/products');
    }

    public function destroy($id){
        $this->productRepository->destroy($id);
        return redirect()->back();
    }

    public function createProductsIndex() {
        $products = Product::all();

        $params = array();
        $params['index'] = 'products';

        $client = new Elasticsearch\Client();
        if($client->indices()->exists($params)) {
            $client->indices()->delete($params);
        }


        foreach ($products as $product) {
            $colors = array();
            foreach($product->colors as $color) {
                $colors[] = $color->name;
            }

            $data = array(
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'brand' => $product->brand->name,
                'description' => $product->description,
                'pictures' => $product->pictures,
                'colors' => $colors,
                'stock' => $product->stock
            );
            $params['body'] = $data;
            $params['type'] = 'all';
            $params['id'] = $product->id;

            $client->index($params);
        }

        \Session::flash('flash_message', 'Les produits ont été indexés');

        return redirect('/admin/products');
    }
}
