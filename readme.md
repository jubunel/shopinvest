# ShopInvest technical test

## Installation
```
git clone git@bitbucket.org:jbunel/shopinvest.git
```

```
composer install
```

```
cp .env.example .env
```

Créer une base de données et mettre à jour le .env pour configurer l'accès à la base de données

```
php artisan key:generate
```

```
php artisan migrate
```

```
chmod -R 777 storage/
```

## Utilisation

Installer et lancer Elastic Search en local sur le port par défaut (9200)

Interface d'adminitsration => http://{your_domain}/admin/products

Front => http://{your_domain}/

1°) Ajouter un ou plusieurs produits via l'interface d'administration

2°) Indexer les produits (Bouton 'Indexer les produits') sinon ils ne s'afficheront pas dans la page catalogue

3°) Aller sur le front pour consulter le catalogue et naviguer sur une page produit

4°) Possibilité d'ajouter un produit au panier

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)