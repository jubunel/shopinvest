<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    public function index() {
        $colors = Color::all();
        return view('colors.index', compact('colors'));
    }

    public function create() {
        return view('colors.add');
    }

    public function store(Request $request) {
        $color = new Color();
        $inputs =  $request->except('_token');
        $color->create($inputs);

        return redirect(route('color.index'));
    }
}
