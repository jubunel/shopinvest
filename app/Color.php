<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model {
    protected $table = 'colors';

    protected $fillable = array('name', 'hexadecimal');

    public function products() {
        $this->belongsToMany('App\Product');
    }
}
