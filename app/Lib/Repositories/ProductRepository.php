<?php
namespace App\Lib\Repositories;

use App\Product;

class ProductRepository {
    protected $product;

    public function __construct(Product $product) {
        $this->product = $product;
    }

    private function queryWithBrandAndColorsAndPictures() {
        return $this->product->with('brand', 'colors', 'pictures')->orderBy('products.created_at', 'desc');
    }

    public function getWithBrandAndColorsAndPicturesPaginate($n) {
        return $this->queryWithBrandAndColorsAndPictures()->paginate($n);
    }

    /*public function getWithUserAndTagsForTagPaginate($tag, $n)
    {
        return $this->queryWithUserAndTags()
            ->whereHas('tags', function($q) use ($tag)
            {
                $q->where('tags.tag_url', $tag);
            })->paginate($n);
    }*/

    public function getById($id) {
        return $this->product->findOrFail($id);
    }

    public function store(Array $inputs) {
        return $this->product->create($inputs);
    }

    public function update($id, Array $inputs) {
        $product = $this->getById($id);

        $product->name = $inputs['name'];
        $product->price = $inputs['price'];
        $product->stock = $inputs['stock'];
        $product->brand_id = $inputs['brand_id'];

        $product->save();
    }

    public function destroy($id) {
        $product = $this->product->findOrFail($id);

        if(sizeof($product->pictures)> 0) {
            foreach($product->pictures as $picture) {
                $path = public_path();
                $picture->delete();
                unlink($path . '/uploads/'.$picture->filename);
            }
        }

        $product->colors()->detach();
        $product->delete();
    }
}