<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Color;
use Illuminate\Http\Request;
use App\Product;
use DB;

use App\Lib\Repositories\ProductRepository;
use App\Http\Requests;
use Elasticsearch;

class MainController extends Controller {
    protected $productRepository;
    protected $nbrPerPage = 10;

    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request) {
        /* Brands select */
        $brandsSelect = array('' => 'choose');
        $brands = Brand::all();
        foreach($brands as $b) {
            $brandsSelect[$b->name] = $b->name;
        }

        /* max price and min price */
        $maxPrice = DB::table('products')->max('price');
        $minPrice = DB::table('products')->min('price');

        $colorsSelect = array('' => 'choose');
        $colors = Color::All();
        foreach ($colors as $color) {
            $colorsSelect[$color->name] = $color->name;
        }

        $total = 0;
        $cartContent = array();

        // Cart content
        if ($request->session()->has('cart')) {
            $cartContent = $request->session()->get('cart');

            if(count($cartContent) > 0) {
                foreach($cartContent as $item) {
                    $total += (int)$item['qty'];
                }
            }
        }

        $products = $this->productRepository->getWithBrandAndColorsAndPicturesPaginate($this->nbrPerPage);

        $links = $products->setPath('')->render();
        return view('front.index',compact('products', 'links', 'cartContent', 'total', 'brandsSelect', 'minPrice', 'maxPrice', 'colorsSelect'));


    }

    public function show(Request $request, $id) {
        $product = $this->productRepository->getById($id);
        $cartContent = array();
        $total = 0;

        // Cart content
        if ($request->session()->has('cart')) {
            $cartContent = $request->session()->get('cart');

            if(count($cartContent) > 0) {
                foreach($cartContent as $item) {
                    $total += (int)$item['qty'];
                }
            }
        }
        return view('front.product', compact('product', 'cart', 'total'));


    }

    public function addItem(Request $request, $id, $qty) {
        $product = Product::find($id);
        $data = array("id" => $product->id, "qty" => $qty, "total" => $qty*$product->price);

        if ($request->session()->has('cart')) {

            $cart = $request->session()->get('cart');
            array_push($cart, $data);
            $request->session()->put('cart',$cart);
        } else {
            $request->session()->put('cart', array($data));
        }

        return redirect('/product/detail/'.$product->id);
    }

    public function search(Request $request) {
        /* Brands select */
        $brandsSelect = array('' => 'choose');
        $brands = Brand::all();
        foreach($brands as $b) {
            $brandsSelect[$b->name] = $b->name;
        }

        /* max price and min price */
        $maxPrice = DB::table('products')->max('price');
        $minPrice = DB::table('products')->min('price');

        $colorsSelect = array('' => 'choose');
        $colors = Color::All();
        foreach ($colors as $color) {
            $colorsSelect[$color->name] = $color->name;
        }


        $inputs = $request->all();
        $price = $inputs['price'];
        $colors = $inputs['colors'];
        $stock = isset($inputs['stock']) ? 1 : 0;
        $brand = $inputs['brand'];
        $total  = 0;
        $products = array();

        // Get products with ES
        $params = array();
        $params['index'] = 'products';
        $params['type'] = 'all';

        $search = array();

        if(!empty($brand))
            $search[] = [ 'match' => [ 'brand' => $brand ] ];


        if(!empty($price)) {
            $price = explode(",", $price);
            $params['body']['query']['filtered']['filter']['range']['price']['gte'] = (int)$price[0]-1;
            $params['body']['query']['filtered']['filter']['range']['price']['lte'] = (int)$price[1]+1;
        }


        if(!empty($colors)) {
            $search[] = [ 'match' => [ 'colors' => $colors ] ];
        }

        $search[] = [ 'match' => [ 'stock' => $stock ] ];
        //$search['stock'] = $stock;

        $params['body']['query']['filtered']['query']['bool']['must'] = $search;
        //$params['body']['query']['filtered']['filter']['terms'] = $search;

        $client = new Elasticsearch\Client();
        $result = $client->search($params);
        $products = $result['hits']['hits'];

        // Cart content
        if ($request->session()->has('cart')) {
            $cartContent = $request->session()->get('cart');

            if(count($cartContent) > 0) {
                foreach($cartContent as $item) {
                    $total += (int)$item['qty'];
                }
            }
        }
        return view('front.search',compact('products', 'cartContent', 'total', 'brandsSelect', 'inputs', 'minPrice', 'maxPrice', 'colorsSelect'));
    }
}
