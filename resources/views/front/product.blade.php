@extends('layouts.front')

@section('title')
    Produit {{$product->name}}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">Accueil</a></li>
        <li class="active">{{$product->name}}</li>
    </ol>
@endsection

@section('cart')
    @if($total == 0)
        Aucun article
    @else
        {{ $total }} article(s)
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <a href="#" class="thumbnail">
                        @if(sizeof($product->pictures) > 0)
                        <img src="/uploads/{{$product->pictures[0]->filename}}" class="img-responsive" id="main">
                        @endif
                    </a>
                </div>
            </div>
            <div class="row">
                @if(sizeof($product->pictures) > 0)
                    @for ($i = 0; $i < sizeof($product->pictures); $i++)
                        @if($i > 0)
                            <div class="col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="/uploads/{{$product->pictures[$i]->filename}}" class="img-responsive miniature" >
                                </a>
                            </div>
                        @endif
                    @endfor
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="row bordered">
                <div class="col-md-6">
                    <h2>{{$product->name}}</h2>
                    <h3>{{$product->brand->name}}</h3>
                </div>
                <div class="col-md-6">
                    <div class="price">{{$product->price}} €</div>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
                        <li role="presentation"><a href="#livraison" aria-controls="livraison" role="tab" data-toggle="tab">Livraison</a></li>
                        <li role="presentation"><a href="#garanties" aria-controls="garanties" role="tab" data-toggle="tab">Garantie & paiement</a></li>
                        <li role="presentation"><a href="#txt" aria-controls="txt" role="tab" data-toggle="tab">Txt</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="description">
                            <div class="colors">
                                @if(count($product->colors) > 0)
                                    Coloris :
                                    @for ($i = 0; $i < count($product->colors); $i++)

                                        {{ $product->colors{$i}->name }}@if($i+1 != count($product->colors)),@endif
                                    @endfor
                                @endif
                            </div><br />
                            <div class="desc">{{$product->description}}</div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="livraison">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquam lectus non vehicula elementum. Aliquam erat volutpat. Sed id enim in sem tristique dignissim. Sed ultricies risus non nisl convallis, ultricies cursus nisl fermentum. Cras semper fringilla aliquet. Curabitur suscipit consequat felis, a vehicula neque laoreet at. Curabitur diam enim, facilisis nec lacinia nec, blandit sed arcu. Duis eget ipsum et dui pellentesque placerat.</div>
                        <div role="tabpanel" class="tab-pane" id="garanties">Mauris vel ultricies sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent at erat augue. Nulla mauris justo, mattis vitae tempus eget, pellentesque iaculis ante. Proin sit amet mattis lectus, eget feugiat elit. Duis pellentesque mattis nisi. Nam tincidunt turpis eu odio fermentum, nec iaculis odio ullamcorper. Nunc feugiat, metus at iaculis vulputate, libero ipsum luctus urna, nec porttitor justo turpis et nulla. Sed pellentesque cursus elit ut gravida. Nullam a lacinia leo, eget egestas lacus. Vestibulum interdum, tortor vel accumsan imperdiet, mi velit gravida tellus, at molestie felis augue a lorem. Morbi eget magna ac tortor mollis tempus. Integer et nunc neque.</div>
                        <div role="tabpanel" class="tab-pane" id="txt">Maecenas est diam, iaculis id est sagittis, scelerisque blandit leo. Nullam vulputate tortor vel nunc bibendum, vel consectetur erat suscipit. Curabitur egestas nibh at dignissim eleifend. Vivamus quis mauris faucibus, faucibus lectus a, aliquam nibh. Fusce imperdiet nibh a tristique mollis. Aenean aliquam viverra ullamcorper. Aliquam erat volutpat. Pellentesque laoreet ornare turpis eget ultrices.</div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 40px; line-height: 40px;">
                <div class="col-md-4 bordered">
                    Quantité
                </div>
                <div class="col-md-4">
                    <ul class="addToCart">
                        <li><a href="#" class="moins bordered" id="minus">-</a></li>
                        <li><a href="#" class="qte bordered" id="qte">1</a></li>
                        <li><a href="#" class="plus bordered" id="plus">+</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <a href="/cart/add/{{$product->id}}/" class="btn btn-default" id="addToCart">Ajouter au panier</a>
                </div>
            </div>
        </div>
    </div>
@endsection