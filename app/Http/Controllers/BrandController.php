<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    public function index() {
        $brands = Brand::all();
        return view('brands.index', compact('brands'));
    }

    public function create() {
        return view('brands.add');
    }

    public function store(Request $request) {
        $color = new Color();
        $inputs =  $request->except('_token');
        $color->create($inputs);

        return redirect(route('color.index'));
    }

    public function getdata() {
        $brands = Brand::all();
        return compact('brands');
    }
}
