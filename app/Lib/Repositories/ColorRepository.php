<?php
namespace App\Lib\Repositories;

use App\Color;

class ColorRepository {
    protected $color;

    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    public function store($product, $colors) {
        $colors = explode(',', $colors);

        foreach($colors as $color) {
            $color = trim($color);

            $color_ref = $this->color->where('name', $color)->first();

            if(is_null($color_ref)) {
                $color_ref = new $this->color([
                    'name' => $color
                ]);

                $product->colors()->save($color_ref);
            } else {
                $product->colors()->attach($color_ref->id);
            }
        }
    }
}