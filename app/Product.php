<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','price', 'description', 'stock', 'brand_id'];

    public function brand() {
        return $this->belongsTo('App\Brand');
    }

    public function pictures() {
        return $this->hasMany('App\Picture');
    }

    public function colors() {
        return $this->belongsToMany('App\Color');
    }
}
