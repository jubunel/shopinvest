@extends('layouts.back')

@section('title', 'Ajouter un produit')

@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="/admin/products">Produits <span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Ajouter un produit</h1>
            {{--<form method="POST" action="/admin/product/save" class="form-horizontal" enctype="multipart/form-data" role="form">--}}
                {!! Form::open(array('route' =>'admin.products.store','method'=>'POST', 'files'=>true, 'class'=>'form-horizontal', 'role'=>'form')) !!}
                {!! csrf_field() !!}
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                        {!! Form::label('name', 'Nom du produit', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Nom du produit']) !!}
                            {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                        {!! Form::label('price', 'Prix', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('price', null, ['class' => 'form-control input-md', 'placeholder' => 'Prix']) !!}
                            {!! $errors->first('price', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('brand_name') ? 'has-error' : '' !!}">
                        {!! Form::label('brand_name', 'Marque', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('brand_name', null, ['class' => 'form-control input-md', 'placeholder' => 'Marque']) !!}
                            {!! $errors->first('brand_name', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                        {!! Form::label('description', 'Description', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('images') ? 'has-error' : '' !!}">
                        {!! Form::label('images', 'Images', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::file('images[]', array('multiple'=>true)) !!}
                            {!! $errors->first('images', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('colors') ? 'has-error' : '' !!}">
                        {!! Form::label('colors', 'Coloris', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('colors', null, ['class' => 'form-control input-md', 'placeholder' => 'Coloris']) !!}
                            {!! $errors->first('colors', '<small class="help-block">:message</small>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label class="col-md-3 control-label" for="stock">Stock</label>
                            <div class="col-md-9">
                                <label>
                                    {!! Form::checkbox('stock', true, false) !!}
                                    En stock
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="submit"></label>
                        <div class="col-md-9">
                            {!! Form::submit('Ajouter un produit', ['class' => 'btn btn-primary']) !!}

                        </div>
                    </div>
                    {!! Form::close() !!}
                </fieldset>
            </form>
        </div>
    </div>
@endsection